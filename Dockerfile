# Use the official Nginx image as the base
FROM nginx:latest

# Create a directory within the container to store files
RUN mkdir -p /usr/share/nginx/html/myapp

# Copy the contents of the local "myapp" folder into the container's directory
COPY myapp /usr/share/nginx/html/myapp

# Set the working directory
WORKDIR /usr/share/nginx/html/myapp

# Expose port 80
EXPOSE 80

# Set the default command to execute when the container starts
CMD ["nginx", "-g", "daemon off;"]

# Set the entry point for the container
ENTRYPOINT ["/docker-entrypoint.sh"]
