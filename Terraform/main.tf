provider "aws" {
    region  = "us-east-1"
    profile = "default"
}

resource "aws_instance" "project_a" {
    instance_type = "t2.micro"
    ami           = "ami-04b70fa74e45c3917"
}
